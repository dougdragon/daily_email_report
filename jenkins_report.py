# -*- coding: utf-8 -*-
import json
import datetime
import requests


base_url = "http://localhost:8080/"


def get_status(job_name):
    """
    Gets the status of the specified test and returns the json data
    PARAMETER: job_name: the job name is retrieved from the get_job_name method
    """
    job_url = base_url + "job/%s/lastBuild/api/json" % job_name
    try:
        r = requests.get(job_url)
    except:
        raise Exception("Jenkins is not responding. Perhaps the Jenkins service is not running?")
    # if r.status_code != 200:
    #     raise Exception("Jenkins returned %s code. "
    #                     "Perhaps Jenkins not running?" % r.status_code)
    try:
        status_json = json.loads(r.content)
    except:
        raise Exception("Failed to parse json data")
    return status_json


def generate_report():
    """ Generates the html and fills in the data from the Jenkins status """
    report = open('c:\\dev\\automation\\jenkins_report\\report.html', 'w')
    report.writelines('<html>'
                      '<head>'
                      '<link href="assets/bootstrap.min.css" rel="stylesheet">'
                      '<link href="assets/bootstrap-responsive.css" rel="stylesheet">'
                      '<meta name="viewport" content="width=device-width, initial-scale=1.0">'
                      '<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />'
                      '</head>'
                      '<body><center><h1>Automation Test Results</h1></center>'
                      '<table class="table table-striped table table-bordered tabl table-condensed table table-hover" border="1" align="center" width="550px">'
                      '<tr><td align="center"><strong>Test Name</strong></td><td align="center"><strong>Status</strong></td><td align="center" width="25"><strong>Duration</strong></td></tr>')
    report.close()
    jobs = get_job_names()
    total_duration = 0.0
    test_count = 0
    fill_report = open('c:\\dev\\automation\\jenkins_report\\report.html', 'a')
    for job in jobs:
        status_data = get_status(job)
        if "jenkins" not in status_data['fullDisplayName']:
            name = job
            result = status_data['result']
            duration = status_data['duration'] / 1000.00
            url = status_data['url']
            total_duration += duration
            if duration > 60:
                duration = duration / 60
                duration = "%.2f minutes" % duration
            else:
                duration = "%.2f seconds" % duration
            test_count += 1
            html_head_content = ('<tr><td align="center">%s</td>'
                                 '<td align="center"><a href="%s">%s</a></td>'
                                 '<td align="center">%s</tr>' % (name, url, result, duration))
            fill_report.writelines(html_head_content)
        else:
            print ""  # ignore this test in the report
    if total_duration > 60:
        total_duration = total_duration / 60
        total_duration = "%.2f minutes" % total_duration
    else:
        total_duration = "%.2f seconds" % total_duration

    html_body = ('</table><table class="table table-condensed" border="0" '
                 'align="center" width="550px"><tr colspan="3"><td '
                 'align="center"><strong>Test statistics</strong></td></tr>'
                 '<tr><td>Duration total: %s</td></tr><tr><td>'
                 'Number of tests: %s </td></tr></table><div '
                 'id="fixed-footer"><div class="container"><p'
                 'class="muted credit">This report was generated at <strong>%s'
                 '</strong></p><p>ヽ[ ﾟヮ・]ノ.･ﾟ｡･+☆ |</p></div></div></body>'
                 '</html>' % (total_duration, test_count, datetime.datetime.now()))

    fill_report.writelines(html_body)

def get_job_names():
    jenkins_url = base_url + "api/json/jobs"
    job_names = []
    try:
        jenkins_request = requests.get(jenkins_url)
    except:
        raise Exception("Jenkins is not responding. Perhaps the Jenkins service is not running?")
    try:
        jobs = json.loads(jenkins_request.content)
    except:
        raise Exception("Failed to parse json data")
    for job in jobs['jobs']:
        job_names.append(job['name'])
    return job_names

generate_report()
